# Amazon DynamoDB Connector
"Amazon DynamoDB is a fully managed NoSQL database service that provides fast and predictable performance with seamless scalability."

Documentation: https://docs.aws.amazon.com/dynamodb/

Specification: https://github.com/APIs-guru/openapi-directory/blob/main/APIs/amazonaws.com/dynamodb/2012-08-10/openapi.yaml

## Prerequisites

+ A DynamoDB instance
+ Generate an AWS Acess Key ID and AWS Secret Key from the AWS Admin Console

## Supported Operations
All endpoints are operational.
## Connector Feedback

Feedback can be provided directly to Product Management in our [Product Feedback Forum](https://community.boomi.com/s/ideas) in the boomiverse.  When submitting an idea, please provide the full connector name in the title and a detailed description.

